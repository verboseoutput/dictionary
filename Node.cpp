#include "Node.h"

/**
 * if the key is a child of the node, returns the associated node
 * otherwise returns a nullptr
 */
Node* Node::getChild(char key) { 
    auto it = m_children.find(key);
    if(it == m_children.end()) {
        return nullptr;
    } else {
        return (*it).second;
    }
}