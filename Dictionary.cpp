#include <algorithm>
#include <iostream>

#include "Dictionary.h"

/**
 * adds a words in the subtree of node to list 'words'
 */
void Dictionary::wordsInSubtree(Node* node, std::list<std::string>& words) {
    std::string word = node->getWord();
    if(!word.empty()) {
        words.push_back(word);
    }
    for(auto keyNode : node->getChildren()) {
        wordsInSubtree(keyNode.second, words);
    }
}

/**
 * adds a list of words to our dictionary struct
 */
void Dictionary::addWords(std::list<std::string> words) {
    // for every word in our list
    std::string::iterator it;
    for(auto word : words) {
        if(word.empty()) continue; // remove empty strings

        std::transform(word.begin(), word.end(), word.begin(), ::tolower);

        bool containsWord = false;
        it = word.begin();
        Node* parentNode = m_root;
        Node* childNode = parentNode->getChild(*it);
        // check if our list already contains a node with each letter
        while(childNode != nullptr) {
            ++it;
            // our dictionary already has that word
            if(it == word.end()) {
                if(childNode->getWord() != word) {
                    childNode->setWord(word);
                }
                containsWord = true;
                break;
            }
            parentNode = childNode;
            childNode = childNode->getChild(*it);
        }
        if(containsWord) continue;

        // add the rest of the letters to our data structure
        while(it != word.end()) {
            childNode = new Node(*it);
            parentNode->addChild(childNode);
            parentNode = childNode;

            ++it;
        }
        // put the word value in the last node
        parentNode->setWord(word);   
    }
}

/**
 * see if a word is in our dictionary
 */
bool Dictionary::hasWord(std::string word) {
    if(word.empty()) return false;

    auto it = word.begin();
    Node* node = m_root;
    node = node->getChild(*it);
    while(node != nullptr) {
        ++it;
        // if we've reached the end of the word w/o hitting a leaf AND
        // the last node contains our word, the dict contains the word
        if(it == word.end() && word == node->getWord()){
            return true; 
        }
        node = node->getChild(*it);
    }
    return false;
}

/**
 * returns all words in the dictionary which start with 'substring'
 */
std::list<std::string> Dictionary::wordsWhichStartWith(std::string substring) {
    std::list<std::string> answer;
    if(substring.empty()) return answer;

    auto it = substring.begin();
    Node* node = m_root;
    node = node->getChild(*it);
    while(node != nullptr) {
        ++it;
        // if we've reached the end of the word w/o hitting a leaf
        // call words in subtree on this node
        if(it == substring.end()){
            wordsInSubtree(node, answer);
            break; 
        }
        node = node->getChild(*it);
    }
    return answer;
}

std::list<std::string> Dictionary::words(){
    std::list<std::string> answer;
    wordsInSubtree(m_root, answer);
    return answer;
}