#ifndef NODE_H_
#define NODE_H_

#include <unordered_map>

/**
 * A node class for constructing Tris
 * m_key is used for identifying nodes
 * m_word is an extra value useful to our dictionary class
 */
class Node {
protected:
    char m_key;
    std::string m_word;
    std::unordered_map<char,Node*> m_children;

public:
    Node(char key) : m_key(key) {}
    ~Node() {
        for(auto child : m_children) {
            delete(child.second);
        }
    }
    
    char getKey() { return m_key; }
    std::string getWord() { return m_word; }
    // returns child with associated key if they exist
    // nullptr otherwise
    Node* getChild(char key);
    std::unordered_map<char,Node*> getChildren() { return m_children; }

    void setWord(std::string word) { m_word = word; }
    void addChild(Node* child) { m_children[child->getKey()] = child; }
};

#endif