#ifndef DICTIONARY_H_
#define DICTIONARY_H_

#include <string>
#include <list>

#include "Node.h"

/**
 * a Tri for storing words where each word is a path
 * from the root to a leaf. Thus, "Tom" and "Toll" would shared nodes
 * 't' and 'o'. And the 'o' node would have children 'm' and 'l'(which would have a child 'l').
 * The root is effectively an empty node pointing to the start of each of
 * the possible words
 * if a substring of a word is also a word a non leaf node may have a word value.
 * So for 'tim' and 'time', The node for 'm' will have m_word = 'tim' and a child
 * node 'e' which has an m_word = 'time'
 */
class Dictionary {
    Node* m_root;

    // returns a list of words contained in the subtree of 'node'
    // takes a reference to a list because it's a recursive function
    // and we don't want to keep creating lists
    void wordsInSubtree(Node* node, std::list<std::string>& words);

public:
    Dictionary() { m_root = new Node(' '); }
    ~Dictionary() { delete(m_root); }

    // returns the root node
    Node* operator()(){ return m_root; }
    
    // add a list of words to the structure
    void addWords(std::list<std::string> words);
    // return whether or not the dictionary contains the word
    bool hasWord(std::string word);
    // returns a list of words which start with a substring
    std::list<std::string> wordsWhichStartWith(std::string substring);
    // returns a list of all words in the struct
    std::list<std::string> words();
    
};

#endif