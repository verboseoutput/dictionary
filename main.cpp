#include <list>
#include <string>
#include <iostream>

#include "Dictionary.h"

using namespace std;
int main() {
    list<string> wordList;
    wordList.push_back("hello");
    wordList.push_back("hell");
    wordList.push_back("happy");
    wordList.push_back("tree");
    wordList.push_back("trees");
    wordList.push_back("and");

    Dictionary dict;
    dict.addWords(wordList);

    cout << "all words" << endl;
    for(auto word : dict.words()) {
        cout << word << endl;
    }
    cout << endl;

    cout << "words which begin with h*" << endl;
    for(auto word : dict.wordsWhichStartWith("h")) {
        cout << word << endl;
    }
    cout << endl;

    cout << "dict contains:" << endl;
    cout << "and: " << dict.hasWord("and") << endl;
    cout << "at: " << dict.hasWord("at") << endl;

    return 0;
}