CC = g++
CFLAGS = -c -Wall -g -std=c++11

OBJDIR = obj

EXE = prog
SRCS = $(wildcard *.cpp)
OBJS = $(patsubst %.cpp, $(OBJDIR)/%.o, $(SRCS))

$(EXE): $(OBJS)
	$(CC) $(OBJS) -o $(EXE) $(LIBS)

$(OBJS): | obj

obj:
	@mkdir -p $@ 
	
$(OBJDIR)/%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@ $(LIBS)
	
	
.PHONY: clean
	
clean:
	-rm $(OBJS)
	-rm $(EXE)
